var cvs;
var gui = {};
// starting position 
var xi, yi;
// initial angle
var ai = 0;
var alive = 1;
var branch1 = 0;
var branch2 = 0;
const PI_2 = Math.PI * 0.5;
const rate = 0.05;

function get()
{
    // display slider values in html text:
    gui.objW.previousElementSibling.value = gui.objW.value;
    gui.objL.previousElementSibling.value = gui.objL.value;
    gui.objA.previousElementSibling.value = gui.objA.value;
    gui.objD.previousElementSibling.value = gui.objD.value;
    // get values
    gui.W = gui.objW.value;
    gui.L = gui.objL.value;
    gui.A = gui.objA.value * Math.PI / 180;
    gui.N = gui.objD.value / 180;
    gui.D = 10;
}

function init()
{
    cvs = canvas.getContext('2d');
    // full screen canvas!
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;    
    setInterval(draw, 10);

    gui.objW = document.getElementById("sliderW");
    gui.objL = document.getElementById("sliderL");
    gui.objA = document.getElementById("sliderA");
    gui.objD = document.getElementById("sliderD");
    
    xi = 0.5 * canvas.width;
    yi = 0.5 * canvas.height;
    get();
}

function stop()
{
	alive = !alive;
}

function disc(X, Y, R, fill, stroke)
{
    cvs.beginPath();
    cvs.fillStyle = fill;
    cvs.strokeStyle = stroke;
    cvs.arc(X, Y, R, 0, 2*Math.PI);
    cvs.closePath();
    cvs.fill();
    cvs.stroke();
}

function oval(X, Y, C, S, L, R, ang, color)
{
    let U = X + L * C;
    let V = Y + L * S;
    let Rx = R * C;
    let Ry = R * S;
    cvs.beginPath();
    cvs.fillStyle = 'rgb(0, '+color+', 0.5)';
    //cvs.fillStyle = 'rgb(255, 165, 0)';
    cvs.moveTo(X+Ry, Y-Rx);
    cvs.lineTo(U+Ry, V-Rx);
    cvs.arc(U, V, R, ang-PI_2, ang+PI_2);
    cvs.lineTo(U-Ry, V+Rx);
    cvs.lineTo(X-Ry, Y+Rx);
    cvs.arc(X, Y, R, ang+PI_2, ang-PI_2);
    cvs.closePath();
    cvs.fill();
}


function noise()
{
    return gui.N * ( Math.random() - 0.5 );
}

function sound()
{
    return 1 + gui.N * Math.random();
}

class Cell
{
    constructor(l, r, a, d=0) { 
        this.len = l / sound(); // length of the element
        this.rad = r / sound(); // radius of the element
        this.angle = a + Math.PI * noise(); // angle relative to mother
        this.child1 = 0;
        this.child2 = 0;
        this.depth = d+1;
        this.age = 0;
        this.adult1 = gui.L / sound();
        this.adult2 = gui.L / sound();
    }

    evolve() {
        this.age += 1;
        let R = rate / this.depth;
        this.rad += ( gui.W - this.rad ) * R * 0.5;
        this.len += ( gui.L - this.len ) * R;
        if ( this.depth < gui.D )
        {
            if ( this.child1 ) 
                this.child1.evolve();
            else if ( this.len >= this.adult1 )
                this.child1 = new Cell(0, this.rad*0.4,-1, this.depth)
            if ( this.child2 )
                this.child2.evolve();
            else if ( this.len >= this.adult2 )
                this.child2 = new Cell(0, this.rad*0.4, 1, this.depth)
        }
    }
    
    paint(X, Y, ang) {
        //disc(x, y, this.rad+1, "#00AA00", "#004400");
        ang += gui.A * this.angle;
        let C = Math.cos(ang), S = Math.sin(ang)
        let color = 8*this.depth + 192*Math.exp(-this.age*0.001);
        oval(X, Y, C, S, this.len, this.rad, ang, color)
        let Rx = this.rad * C;
        let Ry = this.rad * S;
        let RL = this.len + this.rad * 0.5;
        let U = X + RL * C;
        let V = Y + RL * S;
        if ( this.depth < gui.D )
        {
        	if ( this.child1 ) this.child1.paint(U+0.5*Ry, V-0.5*Rx, ang);
        	if ( this.child2 ) this.child2.paint(U-0.5*Ry, V+0.5*Rx, ang);
        }
    }
    
    stroke(X, Y, ang) {
        ang += gui.A * this.angle;
        let C = Math.cos(ang), S = Math.sin(ang)
        let Rx = this.rad * C;
        let Ry = this.rad * S;
        let RL = this.len + this.rad * 0.5;
        let U = X + RL * C;
        let V = Y + RL * S;
        cvs.lineTo(X+Ry, Y-Rx);
        //cvs.lineTo(U+Ry, V-Rx);
        if ( this.depth < gui.D )
        {
	        if ( this.child1 )
                this.child1.stroke(U+0.5*Ry, V-0.5*Rx, ang);
            else
                cvs.arc(U, V, this.rad, ang-PI_2, ang);   
    	    if ( this.child2 )
                this.child2.stroke(U-0.5*Ry, V+0.5*Rx, ang);
            else
                cvs.arc(U, V, this.rad, ang, ang+PI_2);
        }
        else cvs.arc(U, V, this.rad, ang-PI_2, ang+PI_2);
        //cvs.lineTo(U-Ry, V+Rx);
        cvs.lineTo(X-Ry, Y+Rx);
    }
}

function reset()
{
    branch1 = new Cell(1, 1, 0)
    branch2 = new Cell(1, 1, 0)
    alive = 1;
}

function draw()
{
    cvs.clearRect(0, 0, canvas.width, canvas.height);
    if ( branch1 ) {
        if ( alive ) branch1.evolve();
        //branch1.paint(xi, yi, ai);
        
        cvs.beginPath();
        cvs.moveTo(xi, yi);
        cvs.strokeStyle = "yellow";
        branch1.stroke(xi, yi, ai);
        cvs.lineTo(xi, yi);
        cvs.stroke();        
    }
    if ( branch2 ) {
        if ( alive ) branch2.evolve();
        branch2.paint(xi, yi, ai+Math.PI);
    }
}
